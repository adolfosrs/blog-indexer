# Blog Indexer

Yet another blog indexer tool. Categorizes and searches content of blogs.

## How to configure Eclipse IDE and run the App

1. Download an install [Eclipse Java IDE Juno](http://www.eclipse.org/downloads/packages/eclipse-ide-java-developers/junor)
2. Put the Blog Indexer files in the workspace directory
(usually $USER_HOME/workspace/blog-indexer)
3. Create a project on Eclipse and name it "blog-indexer" (it's the 
name you choosed for the folder containing the files of the Blog Indexer)
4. Right-click on the project entry on the left sidebar in Eclipse and 
select Build Path > Configure Build Path
5. Click in the Libraries tab, Add Jars and select all the jars in 
the lib directory of the project and click OK on the 2 windows to confirm the actions
6. Click Run in the top panel and select Run Configurations
7. Click in the Classpath tab, select User Entries and click in the Advanced button
8. Click in Add Folders button, select the Resources folder (inside the blog-indexer folder),
click in the Apply button and close the window.
9. Run the BlogIndexer class (inside the br.ufrgs.inf.blogindexer.ui package)

## Dependencies

* [JSON-Simple](http://code.google.com/p/json-simple/) - A nice library for JSON manipulation
* [JSON-Java](http://www.json.org/java/) - Library for JSON manipulation
* [JSON-Lib](http://json-lib.sourceforge.net/) - It's getting boring...

## Authors

* Adolfo Henrique Schneider
* Lucas José Kreutz Alves

## License

The good and old MIT License. See the license.txt file in the root folder.

------------------------------------------------------------------

Data Classification, Computer Science

Classificacao e Pesquisa de Dados, Ciencia da Computacao   
Universidade Federal do Rio Grande do Sul (UFRGS), Brasil, 2012
