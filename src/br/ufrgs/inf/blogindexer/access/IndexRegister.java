/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.access;

import java.io.Serializable;

public class IndexRegister implements Comparable<IndexRegister>, Serializable {
	private static final long serialVersionUID = 1L;
	private final String key;
	private final String address;

	public IndexRegister(String key, String address) {
		super();
		this.key = key;
		this.address = address;
	}

	public String getKey() {
		return key;
	}

	public String getAddress() {
		return address;
	}

	/**
	 * Compares this register with another based on the key
	 * 
	 * @param otherRegister
	 * @return negative value if the other is greater, zero if they are equal
	 *         and a positive value if this key is greater than the other
	 */
	@Override
	public int compareTo(IndexRegister other) {
		if (other.getKey().matches("-?\\d+(.\\d+)?") && key.matches("-?\\d+(.\\d+)?")) {
			Double difference = Double.parseDouble(key) - Double.parseDouble(other.getKey());
			int result = 0;
			if (difference > 0) {
				result = 1;
			} else if (difference < 0) {
				result = -1;
			} else {
				result = 0;
			}
			return result;
		} else {
			return key.compareToIgnoreCase(other.getKey());
		}
	}
}
