/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.downloader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import br.ufrgs.inf.blogindexer.access.IndexRegister;
import br.ufrgs.inf.blogindexer.access.SequentialIndex;
import br.ufrgs.inf.blogindexer.config.Context;

public class DownloadController {
	private ArrayList<String> feedURLs = new ArrayList<String>();

	public void addfeedURL(String feedURL) {
		feedURLs.add(feedURL);
	}

	public void get() {
		try {
			for (String feed : feedURLs) {
				// Makes the hash of the name to generate an unique name
				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(feed.getBytes());
				byte[] hashMd5 = md.digest();

				// Start the downloader
				String fileName = stringHexa(hashMd5) + ".xml";
				String localPath = Context.feedsDir + fileName;
				Downloader downloader = new Downloader(feed, localPath);

				// Downloads the file
				downloader.get();

				// Add the file to the index
				SequentialIndex index = new SequentialIndex("feeds.ind");
				index.load();
				if (!index.contains(feed)) {
					index.add(new IndexRegister(feed, fileName));
					index.save();
				}
			}

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Downloaded all the feeds.");
	}

	/**
	 * Convert an array of bytes to string
	 * 
	 * @author http://codare.net/2007/02/02/java-gerando-codigos-hash-md5-sha/
	 * @param bytes
	 * @return string representation of the array of bytes
	 */
	private static String stringHexa(byte[] bytes) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
			int parteBaixa = bytes[i] & 0xf;
			if (parteAlta == 0)
				s.append('0');
			s.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return s.toString();
	}
}
