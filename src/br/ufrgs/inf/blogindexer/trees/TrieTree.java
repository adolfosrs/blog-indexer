/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.trees;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Set;

import br.ufrgs.inf.blogindexer.config.Context;

public class TrieTree {

	TrieNode root = new TrieNode();
	private String treeFilePath = "";

	public TrieTree() {
	}

	public TrieTree(String filename) {
		treeFilePath = Context.treesDir + filename;
	}

	public TrieNode getRoot() {
		return root;
	}

	/**
	 * Add a word to the trie-tree and sets the file in the leaf
	 * 
	 * @param word
	 * @param file
	 */
	public void addWord(String word, String file) {
		int size = word.length();

		TrieNode parent = root;
		TrieNode newChild = null;

		for (int i = 0; i < size; i++) {
			newChild = new TrieNode(word.substring(i, i + 1));
			if (parent.getChildren().contains(newChild)) {
				for (TrieNode child : parent.getChildren()) {
					if (child.compareTo(newChild) == 0) {
						parent = child;
						break;
					}
				}
			} else {
				parent.addChild(newChild);
				parent = newChild;
			}
		}

		parent.addFile(file);
	}

	/**
	 * Checks if the word is set in the trie-tree
	 * 
	 * @param word
	 * @return true if the trie contains the word, false if not
	 */
	public boolean containsWord(String word) {
		int size = word.length();

		TrieNode parent = root;
		boolean found = false;

		for (int i = 0; i < size; i++) {
			found = false;

			for (TrieNode child : parent.getChildren()) {
				if (child.getContent().compareTo(word.substring(i, i + 1)) == 0) {
					parent = child;
					found = true;
					break;
				}
			}

			if (!found) {
				return false;
			}
		}

		if (parent.isWord()) {
			return true;
		}

		return false;
	}

	/**
	 * Returns the files where the word is
	 * 
	 * @param word
	 * @return A set of files
	 * @throws Exception
	 *             If the word is not set in the tree
	 */
	public Set<String> getFilesOfWord(String word) throws Exception {
		int size = word.length();

		TrieNode parent = root;
		boolean found = false;

		for (int i = 0; i < size; i++) {
			found = false;

			for (TrieNode child : parent.getChildren()) {
				if (child.getContent().compareTo(word.substring(i, i + 1)) == 0) {
					parent = child;
					found = true;
					break;
				}
			}

			if (!found) {
				throw new Exception("The word is not set in the tree");
			}
		}

		if (!parent.isWord()) {
			throw new Exception("The word is not set in the tree");
		}

		return parent.getFiles();
	}

	/**
	 * Serializes the tree
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public synchronized void save() throws FileNotFoundException, IOException {
		FileOutputStream indexFile = new FileOutputStream(treeFilePath);
		ObjectOutputStream out = new ObjectOutputStream(indexFile);
		out.writeObject(root);
		out.flush();
		out.close();
	}

	/**
	 * Unserializes the tree
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws ClassCastException
	 */
	public void load() throws IOException, ClassNotFoundException, ClassCastException {
		File path = new File(treeFilePath);
		if (path.exists()) {
			FileInputStream indexFile = new FileInputStream(treeFilePath);
			ObjectInputStream in = new ObjectInputStream(indexFile);
			root = (TrieNode) in.readObject();
			in.close();
		}
	}
}
