/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;

import br.ufrgs.inf.blogindexer.access.IndexRegister;
import br.ufrgs.inf.blogindexer.access.SequentialIndex;
import br.ufrgs.inf.blogindexer.compression.HuffmanCode;
import br.ufrgs.inf.blogindexer.config.Context;
import br.ufrgs.inf.blogindexer.downloader.DownloadController;
import br.ufrgs.inf.blogindexer.xmlparser.ReadXMLFile;

public class AddFeedsForm extends JInternalFrame {

	JTextArea txtURLsOfFeeds;

	private static final long serialVersionUID = 5005273393803870964L;

	/**
	 * Create the frame.
	 */
	public AddFeedsForm() {
		setTitle("Add Feeds");
		setBounds(100, 100, 596, 208);

		JLabel lblUrlsOfFeeds = new JLabel("URLs of Feeds:");

		JButton btnOkYouCan = new JButton("OK, you can download it");
		btnOkYouCan.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DownloadController dc = new DownloadController();
				String urls[] = txtURLsOfFeeds.getText().split("\\r?\\n");
				for (String url : urls) {
					dc.addfeedURL(url);
				}
				dc.get();

				ReadXMLFile xmlParser = new ReadXMLFile();
				xmlParser.parse();

				// Compress with Huffman
				SequentialIndex index = new SequentialIndex("titles.ind");
				try {
					index.load();
				} catch (ClassNotFoundException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (ClassCastException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} catch (IOException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				for (IndexRegister reg : index.getRegisters()) {
					HuffmanCode hc = new HuffmanCode(Context.postsDir + reg.getAddress());
					try {
						hc.calculateFrequencies();
						hc.buildTree();
						hc.compress();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				dispose();
			}
		});

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(lblUrlsOfFeeds).addPreferredGap(ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(btnOkYouCan).addGap(18).addComponent(btnCancel)).addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 460, GroupLayout.PREFERRED_SIZE)).addContainerGap(33, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(lblUrlsOfFeeds).addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)).addPreferredGap(ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(btnOkYouCan).addComponent(btnCancel)).addContainerGap(26, Short.MAX_VALUE)));

		txtURLsOfFeeds = new JTextArea();
		txtURLsOfFeeds.setText("http://www.cartacapital.com.br/feed/\r\nhttp://www.ubuntudicas.com.br/blog/feed/");
		txtURLsOfFeeds.setLineWrap(true);
		scrollPane.setViewportView(txtURLsOfFeeds);
		getContentPane().setLayout(groupLayout);

	}
}
