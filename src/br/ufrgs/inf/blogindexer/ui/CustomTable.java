/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import javax.swing.table.DefaultTableModel;

public class CustomTable extends DefaultTableModel {
	private static final long serialVersionUID = -8704375423778519176L;

	public CustomTable(Object[][] data, Object[] columnNames) {
		super(data, columnNames);
	}

	public CustomTable(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	@Override
	public boolean isCellEditable(int row, int cols) {
		return false;
	}

}
