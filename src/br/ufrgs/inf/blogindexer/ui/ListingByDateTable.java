/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import br.ufrgs.inf.blogindexer.access.IndexRegister;
import br.ufrgs.inf.blogindexer.access.SequentialIndex;
import br.ufrgs.inf.blogindexer.compression.HuffmanCode;
import br.ufrgs.inf.blogindexer.config.Context;

public class ListingByDateTable extends JInternalFrame {
	private static final long serialVersionUID = 5927572529146636489L;
	private JTable table;

	/**
	 * Create the frame.
	 * 
	 * @throws PropertyVetoException
	 */
	public ListingByDateTable() {
		setMaximizable(true);
		setClosable(true);
		setTitle("Listing by Date");
		setBounds(100, 100, 696, 447);

		JPanel panel = new JPanel();

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(null);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(panel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)));

		loadTable('a');
		scrollPane.setViewportView(table);

		JButton btnAscending = new JButton("Ascending");
		btnAscending.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadTable('a');
			}
		});
		panel.add(btnAscending);

		JButton btnDescending = new JButton("Descending");
		btnDescending.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				loadTable('d');
			}
		});
		panel.add(btnDescending);
		getContentPane().setLayout(groupLayout);
	}

	private void loadTable(char order) {
		String[] cols = new String[] { "Date", "Title", "File" };

		if (table == null) {
			table = new JTable();
		} else {
			table.removeAll();
		}

		CustomTable tableModel = new CustomTable(cols, 0);
		table.setModel(tableModel);

		SequentialIndex index;
		if (order == 'd') {
			index = new SequentialIndex("dateDesc.ind");
		} else {
			index = new SequentialIndex("date.ind");
		}

		try {
			index.load();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(ClassCastException e) {
			// TODO Auto-generated catch block
						e.printStackTrace();
		} catch(IOException e){
			// TODO Auto-generated catch block
						e.printStackTrace();
		}
		for (IndexRegister reg : index.getRegisters()) {
			String file = reg.getAddress();
			// String author = reg.getKey();

			JSONParser parser = new JSONParser();

			Date date = new Date(new Long(reg.getKey()));
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String s = formatter.format(date);

			try {

				HuffmanCode hc = new HuffmanCode(Context.postsDir + file);
				Object obj = parser.parse(hc.uncompress());

				JSONObject jsonObject = (JSONObject) obj;

				String title = (String) jsonObject.get("title");

				tableModel.addRow(new Object[] { s, title, file });

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (org.json.simple.parser.ParseException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ClassCastException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int selectedRow = table.getSelectedRow();
					ShowPost viewPost = new ShowPost();

					JSONParser parser = new JSONParser();
					try {
						String file = (String) table.getValueAt(selectedRow, 2);
						HuffmanCode hc = new HuffmanCode(Context.postsDir + file);
						Object obj = parser.parse(hc.uncompress());

						JSONObject jsonObject = (JSONObject) obj;

						SimpleDateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
						Date pubDate = dateParser.parse(jsonObject.get("date").toString());
						Date date = new Date(pubDate.getTime());
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						String formattedDate = formatter.format(date);

						viewPost.setTitleField(jsonObject.get("title").toString());
						viewPost.setAuthorField(jsonObject.get("author").toString());
						viewPost.setTagsField(jsonObject.get("categories").toString());
						viewPost.setDateField(formattedDate);
						viewPost.setCommentsField(jsonObject.get("comments").toString());
						viewPost.setOriginalURLField(jsonObject.get("originalURL").toString());
						viewPost.setContentField(jsonObject.get("content").toString());

					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (org.json.simple.parser.ParseException e1) {
						e1.printStackTrace();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassCastException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					viewPost.setVisible(true);
				}
			}
		});
	}
}
