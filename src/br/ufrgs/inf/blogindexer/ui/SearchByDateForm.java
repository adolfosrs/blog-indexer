/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import br.ufrgs.inf.blogindexer.access.IndexRegister;
import br.ufrgs.inf.blogindexer.access.SequentialIndex;
import br.ufrgs.inf.blogindexer.compression.HuffmanCode;
import br.ufrgs.inf.blogindexer.config.Context;

public class SearchByDateForm extends JInternalFrame {
	private static final long serialVersionUID = -3584303222788291317L;
	private JTextField authorTextField;
	private JTable table;

	/**
	 * Create the frame.
	 */
	public SearchByDateForm() {
		setMaximizable(true);
		setClosable(true);
		setBounds(100, 100, 706, 349);

		JLabel lblAuthor = new JLabel("Date:");

		authorTextField = new JTextField();
		authorTextField.setColumns(10);

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SequentialIndex index = new SequentialIndex("date.ind");
				try {
					index.load();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassCastException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				String[] cols = new String[] { "Date", "Title", "File" };
				CustomTable tableModel = new CustomTable(cols, 0);
				table.setModel(tableModel);
				for (IndexRegister reg : index.getRegisters()) {

					Date postDate = new Date(new Long(reg.getKey()));
					SimpleDateFormat postDateFormatter = new SimpleDateFormat("dd/MM/yyyy");
					String postDateString = postDateFormatter.format(postDate);

					if (postDateString.compareToIgnoreCase(authorTextField.getText()) == 0) {

						// Get today's date
						Date date = new Date(new Long(reg.getKey()));

						// Some examples
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						String s = formatter.format(date);

						JSONParser parser = new JSONParser();

						try {

							HuffmanCode hc = new HuffmanCode(Context.postsDir + reg.getAddress());
							Object obj = parser.parse(hc.uncompress());

							JSONObject jsonObject = (JSONObject) obj;

							String title = (String) jsonObject.get("title");

							tableModel.addRow(new Object[] { s, title, reg.getAddress() });

						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						} catch (org.json.simple.parser.ParseException e1) {
							e1.printStackTrace();
						} catch (ClassNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (ClassCastException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
			}
		});

		JScrollPane scrollPane = new JScrollPane();
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(22).addComponent(lblAuthor).addPreferredGap(ComponentPlacement.UNRELATED).addComponent(authorTextField, GroupLayout.PREFERRED_SIZE, 293, GroupLayout.PREFERRED_SIZE).addPreferredGap(ComponentPlacement.RELATED).addComponent(btnSearch).addContainerGap(257, Short.MAX_VALUE)).addComponent(scrollPane, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblAuthor).addComponent(authorTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(btnSearch)).addGap(9).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE)));

		table = new JTable();
		scrollPane.setViewportView(table);
		getContentPane().setLayout(groupLayout);

		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 2) {
					int selectedRow = table.getSelectedRow();
					ShowPost viewPost = new ShowPost();

					JSONParser parser = new JSONParser();
					try {
						String file = (String) table.getValueAt(selectedRow, 2);
						HuffmanCode hc = new HuffmanCode(Context.postsDir + file);
						Object obj = parser.parse(hc.uncompress());

						JSONObject jsonObject = (JSONObject) obj;

						SimpleDateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
						Date pubDate = dateParser.parse(jsonObject.get("date").toString());
						Date date = new Date(pubDate.getTime());
						SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
						String formattedDate = formatter.format(date);

						viewPost.setTitleField(jsonObject.get("title").toString());
						viewPost.setAuthorField(jsonObject.get("author").toString());
						viewPost.setTagsField(jsonObject.get("categories").toString());
						viewPost.setDateField(formattedDate);
						viewPost.setCommentsField(jsonObject.get("comments").toString());
						viewPost.setOriginalURLField(jsonObject.get("originalURL").toString());
						viewPost.setContentField(jsonObject.get("content").toString());

					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (org.json.simple.parser.ParseException e1) {
						e1.printStackTrace();
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (ClassCastException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					viewPost.setVisible(true);
				}
			}
		});

	}
}
