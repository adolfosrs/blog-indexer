/*******************************************************************************
 * Copyright (c) 2012 Adolfo Schneider, Lucas Kreutz
 * 
 * This code is licensed under MIT License. See the license.txt file
 * or access http://www.opensource.org/licenses/mit-license.php
 ******************************************************************************/
package br.ufrgs.inf.blogindexer.ui;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

public class ShowPost extends JFrame {

	private static final long serialVersionUID = -780657827815716653L;
	private JPanel contentPane;
	private JTextField titleField;
	private JTextField authorField;
	private JTextField tagsField;
	private JTextField dateField;
	private JTextField commentsField;
	private JTextField originalURLField;
	private JScrollPane scrollPane;
	private JTextArea contentField;

	/**
	 * Create the frame.
	 */
	public ShowPost() {
		setTitle("View Post");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 667, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblTtulo = new JLabel("Título:");

		titleField = new JTextField();
		titleField.setEditable(false);
		titleField.setColumns(10);

		JLabel lblAuthor = new JLabel("Author:");

		JLabel lblTags = new JLabel("Tags:");

		JLabel lblDate = new JLabel("Date:");

		JLabel lblcomments = new JLabel("#Comments:");

		JLabel lblOriginalUrl = new JLabel("Original URL:");

		JLabel lblContent = new JLabel("Content:");

		authorField = new JTextField();
		authorField.setEditable(false);
		authorField.setColumns(10);

		tagsField = new JTextField();
		tagsField.setEditable(false);
		tagsField.setColumns(10);

		dateField = new JTextField();
		dateField.setEditable(false);
		dateField.setColumns(10);

		commentsField = new JTextField();
		commentsField.setEditable(false);
		commentsField.setColumns(10);

		originalURLField = new JTextField();
		originalURLField.setEditable(false);
		originalURLField.setColumns(10);

		scrollPane = new JScrollPane();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addComponent(lblTtulo).addGroup(gl_contentPane.createSequentialGroup().addContainerGap().addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING).addComponent(lblTags).addComponent(lblAuthor).addComponent(lblDate).addComponent(lblcomments).addComponent(lblOriginalUrl).addComponent(lblContent)).addPreferredGap(ComponentPlacement.RELATED))).addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(authorField, GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE).addComponent(titleField, GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE).addComponent(tagsField, GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE).addComponent(originalURLField, GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE).addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false).addComponent(commentsField, Alignment.LEADING).addComponent(dateField, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 137, Short.MAX_VALUE)).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE)).addGap(9)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane.createSequentialGroup().addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(titleField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblTtulo)).addPreferredGap(ComponentPlacement.RELATED).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(authorField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblAuthor)).addPreferredGap(ComponentPlacement.RELATED).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(tagsField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblTags)).addGap(8).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(dateField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblDate))).addGroup(gl_contentPane.createSequentialGroup().addGap(106).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(commentsField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblcomments)))).addPreferredGap(ComponentPlacement.RELATED).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(originalURLField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addComponent(lblOriginalUrl)).addPreferredGap(ComponentPlacement.RELATED).addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addComponent(lblContent).addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 264, Short.MAX_VALUE)).addGap(2)));

		contentField = new JTextArea();
		contentField.setLineWrap(true);
		contentField.setEditable(false);
		scrollPane.setViewportView(contentField);
		contentPane.setLayout(gl_contentPane);
	}

	public void setTitleField(String title) {
		this.titleField.setText(title);
	}

	public void setAuthorField(String authorField) {
		this.authorField.setText(authorField);
	}

	public void setTagsField(String tagsField) {
		this.tagsField.setText(tagsField);
	}

	public void setDateField(String dateField) {
		this.dateField.setText(dateField);
	}

	public void setCommentsField(String commentsField) {
		this.commentsField.setText(commentsField);
	}

	public void setOriginalURLField(String originalURLField) {
		this.originalURLField.setText(originalURLField);
	}

	public void setContentField(String content) {
		this.contentField.setText(content);
	}

}
